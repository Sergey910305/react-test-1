import cn from 'classnames';
import React, { FC } from 'react';
import styles from './Input.scss';

interface IInputProps extends React.InputHTMLAttributes<HTMLInputElement> {}

const Input: FC<IInputProps> = ({ children, className, ...props }) => {
  return (
    <input
      {...props}
      className={cn(styles.input, className)}
    >
      {children}
    </input>
  );
};

export default Input;
