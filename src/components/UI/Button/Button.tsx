import cn from 'classnames';
import React, { FC } from 'react';
import styles from './Button.scss';

interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {}

const Button: FC<IButtonProps> = ({ children, className, ...props }) => {
  return (
    <button
      {...props}
      className={cn(styles.button, className)}
    >
      {children}
    </button>
  );
};

export default Button;
