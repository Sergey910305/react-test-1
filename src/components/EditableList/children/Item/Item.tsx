import cn from 'classnames';
import React, {
  FC,
  useCallback,
  useState,
  SyntheticEvent,
  memo,
} from 'react';
import Input from 'components/UI/Input/Input';
import { IItem } from 'interfaces/store/list/store.list.interface';
import styles from './Item.scss';

interface IItemProps {
  item: IItem;
  onDelete: (id: number) => void;
  onEdit: (id: number, input: string) => void;
  onComplete: (id: number) => void;
}

const Item:FC<IItemProps> = ({
  item,
  onDelete: _delete, // delete is reserved in js
  onEdit: edit,
  onComplete: complete,
}) => {
  const [editMode, setEditMode] = useState(false);
  const [value, setValue] = useState(item.name);

  const handleEdit = () => {
    setEditMode(true);
  };

  const handleDelete = () => {
    _delete(item.id);
  };

  const handleComplete = () => {
    complete(item.id);
  };

  const handleEditComplete = () => {
    setEditMode(false);
    edit(item.id, value);
  };

  const handleEditCancel = () => {
    setEditMode(false);
    setValue(item.name);
  };

  const handleInput = useCallback((event: SyntheticEvent) => {
    event.preventDefault();

    const { value } = event.target as HTMLInputElement;

    setValue(value);
  }, []);

  return (
    <li className={cn(styles.li, { [styles.completed]: item.completed })}>
      {
        editMode ?
          <Input
            className={styles.input}
            autoFocus={true}
            value={value}
            onChange={handleInput}
          /> :
          <div title={value} className={styles.name}>{value}</div>
      }
      <div>
        {
          editMode ?
            <>
              <button
                className={cn(styles.button, styles.buttonComplete)}
                onClick={handleEditComplete}>
                [П]
              </button>
              <button className={styles.button} onClick={handleEditCancel}>[О]</button>
            </> :
            <>
              <button className={styles.button} onClick={handleDelete}>[У]</button>
              <button className={styles.button} onClick={handleEdit}>[И]</button>
              <button className={styles.button} onClick={handleComplete}>[З]</button>
            </>
        }
      </div>
    </li>
  );
};

export default memo(Item);
