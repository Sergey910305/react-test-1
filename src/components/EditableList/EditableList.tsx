import React, { FC, memo } from 'react';
import Item from 'components/EditableList/children/Item';
import { IItem } from 'interfaces/store/list/store.list.interface';
import styles from './EditableList.scss';

interface IEditableListProps {
  items: IItem[];
  onDelete: (id: number) => void;
  onEdit: (id: number, input: string) => void;
  onComplete: (id: number) => void;
}

const EditableList:FC<IEditableListProps> = ({
  items,
  onComplete,
  onEdit,
  onDelete,
}) => {
  return (
    <ul className={styles.ul}>
      {items.map((item) => {
        return (
          <Item
            item={item}
            key={item.id}
            onComplete={onComplete}
            onDelete={onDelete}
            onEdit={onEdit}
          />
        );
      })}
    </ul>
  );
};

export default memo(EditableList);
