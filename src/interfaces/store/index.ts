import { IListStore } from './list/store.list.interface';

export interface IStore {
  list: IListStore;
}
