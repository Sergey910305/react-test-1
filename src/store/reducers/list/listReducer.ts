import { AnyAction } from 'redux';
import {
  LIST_ITEM_APPEND,
  LIST_ITEM_EDIT,
  LIST_ITEM_COMPLETE,
  LIST_ITEM_DELETE,
  LIST_AMOUNT_SET,
} from 'store/constants/list';
import { IItem, IListStore } from 'interfaces/store/list/store.list.interface';

const defaultState: IListStore = {
  items: [],
  amount: 0,
};

const findIndexById = (array: IItem[], id: number) => {
  return array.findIndex(item => item.id === id);
};

export default (state = defaultState, action: AnyAction) => {
  switch (action.type) {
    default:
      return state;

    case LIST_AMOUNT_SET:
      return {
        ...state,
        amount: action.amount,
      };

    case LIST_ITEM_APPEND:
      return {
        ...state,
        items: [
          ...state.items,
          action.item,
        ],
      };

    case LIST_ITEM_DELETE: {
      const index = findIndexById(state.items, action.id);
      const items = state.items.slice();

      items.splice(index, 1);

      return {
        ...state,
        items,
      };
    }

    case LIST_ITEM_COMPLETE: {
      const index = findIndexById(state.items, action.id);
      const item = Object.assign({}, state.items[index]);
      const items = state.items.slice();

      item.completed = true;
      items.splice(index, 1, item);

      return {
        ...state,
        items,
      };
    }

    case LIST_ITEM_EDIT: {
      const index = findIndexById(state.items, action.id);
      const item = Object.assign({}, state.items[index]);
      const items = state.items.slice();

      item.name = action.name;
      items.splice(index, 1, item);

      return {
        ...state,
        items,
      };
    }
  }
};
