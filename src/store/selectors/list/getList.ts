import { IStore } from 'interfaces/store';

export const getList = (state: IStore) => state.list;
