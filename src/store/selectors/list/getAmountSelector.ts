import { IStore } from 'interfaces/store';

export const getAmountSelector = (state: IStore) => state.list.amount;
