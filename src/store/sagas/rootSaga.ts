import { all } from 'redux-saga/effects';
import listSaga from './list/listSaga';
import localStorageSaga from './storeRemember/storeRemember';

export default function* rootSaga() {
  yield all([
    ...listSaga,
    ...localStorageSaga,
  ]);
}
