import { takeEvery, select, put } from 'redux-saga/effects';
import { AnyAction } from 'redux';
import { LIST_ITEM_SET } from 'store/constants/list';
import { appendItem, setAmount } from 'store/actions/listActions';
import { getAmountSelector } from 'store/selectors/list/getAmountSelector';
import { IItem } from 'interfaces/store/list/store.list.interface';

function * setItemSaga({ name }: AnyAction) {
  const amountFromStore = yield select(getAmountSelector);
  const amount = amountFromStore + 1;

  yield put(setAmount(amount));
  yield put(appendItem({ name, id: amount } as IItem));
}

export default [
  takeEvery(LIST_ITEM_SET, setItemSaga),
];
