import { IStore } from 'interfaces/store';

export const LOCAL_PERSIST_KEY = 'persistingState';

export const loadState = () => {
  try {
    const persistingState = window.localStorage.getItem(LOCAL_PERSIST_KEY);

    if (persistingState === null) {
      return undefined;
    }
    return JSON.parse(persistingState);
  } catch (err) {
    return undefined;
  }
};

export const saveState = (store: Partial<IStore>) => {
  try {
    window.localStorage.setItem(LOCAL_PERSIST_KEY, JSON.stringify(store));
  } catch (err) {
    // ignore write errors
  }
};
