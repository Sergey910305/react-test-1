import React, { FC, useCallback } from 'react';
import { connect } from 'react-redux';
import InputWithButton from 'components/InputWithButton';
import EditableList from 'components/EditableList';
import {
  setItem,
  editItem,
  deleteItem,
  completeItem,
} from 'store/actions/listActions';
import { IStore } from 'interfaces/store';
import { IItem } from 'interfaces/store/list/store.list.interface';
import styles from './ListContainer.scss';

interface IListContainerMapDispatchToProps {
  setItem: (name: string) => void;
  editItem: (id: number, name: string) => void;
  deleteItem: (id: number) => void;
  completeItem: (id: number) => void;
}

interface IListContainerMapStateToProps {
  items: IItem[];
}

interface IListContainerProps extends
  IListContainerMapDispatchToProps,
  IListContainerMapStateToProps {}

const ListContainer: FC<IListContainerProps> = ({
  editItem: onEditItem,
  deleteItem: onDeleteItem,
  completeItem: onCompleteItem,
  setItem,
  items,
}) => {
  const handleItemAdd = useCallback((name: string) => {
    setItem(name);
  }, []);

  return (
    <div className={styles.container}>
      <div className={styles.containerInner}>
        <InputWithButton
          className={styles.form}
          onSubmit={handleItemAdd}
        />
        <EditableList
          onEdit={onEditItem}
          onDelete={onDeleteItem}
          onComplete={onCompleteItem}
          items={items}
        />
      </div>
    </div>
  );
};

const mapStateToProps = ({ list }: IStore) => {
  return {
    items: list.items,
  };
};

export default connect(mapStateToProps, {
  setItem,
  editItem,
  deleteItem,
  completeItem,
})(ListContainer);
